6.1.0
-----

* The HTML encoder has been made overwhelmingly faster in both the Erlang and JavaScript targets.
  Thanks to Giacomo Cavalieri for the contribution!

6.0.0
-----

* Update for Gleam 0.32+.
* Split encoders and the decoder into separate files so that they can be separately imported.
  This helps keep the file size down.

5.0.0
-----

* Update for Gleam 0.30.

4.0.1
-----

* Fixed a call stack error on the JavaScript target

4.0.0
-----

* Update for Gleam 0.28.

3.1.0
-----

+ Add "HTMLBody" encoding mode that doesn't encode everything.

3.0.0
-----

* Update for Gleam 0.28.
* Enable JavaScript target.

2.0.0
-----

* Update for Gleam 0.27.

1.0.1
-----

* Mark package as Erlang only, waiting for this fix: https://github.com/gleam-lang/gleam/issues/2001

1.0.0
-----

Initial release.
