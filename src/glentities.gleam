import glentities/decoder
import glentities/hex_encoder
import glentities/html_encoder
import glentities/named_encoder

pub type EncodeMode {
  /// Encode all characters that have a specified named entity using that name, except tab and newline.
  Named

  /// Encode all characters using hex entities, except a-z, A-Z, 0-9, newline, tab, carriage return, and space.
  Hex

  /// Encode only the necessary characters when the output target is HTML element or attribute content.
  ///
  /// This means `&`, `<`, `>`, `'`, and `"`.
  ///
  /// Note! Not suitable for outputting inside `<style>`, `<script>` elements.
  HTMLBody
}

/// Decode any HTML entities in the given string.
pub fn decode(text: String) {
  decoder.decode(text)
}

/// Encode characters in text as HTML entities with given encoding mode.
pub fn encode(text: String, mode: EncodeMode) {
  case mode {
    Named -> named_encoder.encode(text)
    Hex -> hex_encoder.encode(text)
    HTMLBody -> html_encoder.encode(text)
  }
}
