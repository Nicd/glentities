@target(erlang)
import glentities/internal/html_encoder/erl as escaper

@target(javascript)
import glentities/internal/html_encoder/generic as escaper

/// Encode text to be safe in the HTML body, inside element or attribute content.
///
/// `&`, `<`, `>`, `'`, and `"` are encoded.
///
/// Note! Not suitable for outputting inside `<style>`, `<script>` elements.
///
pub fn encode(text: String) -> String {
  escaper.escape(text)
}
