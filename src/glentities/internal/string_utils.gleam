@target(erlang)
@external(erlang, "unicode", "characters_to_nfc_binary")
pub fn normalise(text text: String) -> String

@target(javascript)
pub fn normalise(text: String) -> String {
  normalise_js(text, "NFC")
}

@target(javascript)
@external(javascript, "../../glentities_ffi.mjs", "normalize")
pub fn normalise_js(text text: String, mode mode: String) -> String
