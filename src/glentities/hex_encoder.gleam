import gleam/list
import gleam/string
import gleam/int
import glentities/internal/string_utils

/// Encode text using HTML hex entities, except a-z, A-Z, 0-9, newline, tab, carriage return, and space.
pub fn encode(text: String) {
  text
  |> string_utils.normalise()
  |> do_encode()
}

fn do_encode(text: String) {
  text
  |> string.to_utf_codepoints()
  |> list.map(fn(codepoint) {
    case string.utf_codepoint_to_int(codepoint) {
      c if c == 9 || c == 10 || c == 13 || c == 32 || c >= 48 && c <= 57 || c >= 65 && c <= 90 || c >= 97 && c <= 122 ->
        string.from_utf_codepoints([codepoint])
      encodable -> "&#x" <> int.to_base16(encodable) <> ";"
    }
  })
  |> string.join("")
}
