import gleam/string
import gleeunit
import gleeunit/should
import glentities
import glentities/internal/string_utils

pub fn main() {
  gleeunit.main()
}

pub fn decode_test() {
  let input =
    "This &amp &apos;&#115;tring&apos; contains &#128064; many &#xA66E; &quot;encoded&QUOT; characters. &#x2603;"

  should.equal(
    glentities.decode(input),
    "This &amp 'string' contains 👀 many ꙮ \"encoded\" characters. ☃",
  )
}

pub fn encode_named_test() {
  let input =
    "This &amp 'string' contains    many Θ \"encoded\" characters. ☃"

  should.equal(
    glentities.encode(input, glentities.Named),
    "This &amp;amp &apos;string&apos; contains &MediumSpace;&hairsp; many &Theta; &quot;encoded&quot; characters&period; ☃",
  )
}

pub fn encode_hex_test() {
  let input =
    "This &amp 'string' contains    many Θ \"encoded\" characters. ☃"

  should.equal(
    glentities.encode(input, glentities.Hex),
    "This &#x26;amp &#x27;string&#x27; contains &#x205F;&#x200A; many &#x398; &#x22;encoded&#x22; characters&#x2E; &#x2603;",
  )
}

pub fn encode_html_body_test() {
  let input =
    "This &amp 'string' contains    many Θ \"encoded\" <characters>. ☃"

  should.equal(
    glentities.encode(input, glentities.HTMLBody),
    "This &amp;amp &#39;string&#39; contains    many Θ &quot;encoded&quot; &lt;characters&gt;. ☃",
  )
}

pub fn roundtrip_named_test() {
  let input =
    string_utils.normalise(
      "Ἰοὺ ἰού· τὰ πάντʼ ἂν ἐξήκοι σαφῆ. ஸ்றீனிவாஸ ராமானுஜன் ஐயங்கார் A ∩ B = { c : c ∈ A, c ∈ B }",
    )

  should.equal(
    input
      |> glentities.encode(glentities.Named)
      |> glentities.decode(),
    input,
  )
}

pub fn roundtrip_hex_test() {
  let input =
    string_utils.normalise(
      "Ἰοὺ ἰού· τὰ πάντʼ ἂν ἐξήκοι σαφῆ. ஸ்றீனிவாஸ ராமானுஜன் ஐயங்கார் A ∩ B = { c : c ∈ A, c ∈ B }",
    )

  should.equal(
    input
      |> glentities.encode(glentities.Hex)
      |> glentities.decode(),
    input,
  )
}

pub fn tco_test() {
  let input =
    string.repeat(
      "I will never try to be clever in tail call recursive languages again",
      100,
    )

  should.equal(
    input
      |> glentities.encode(glentities.Hex)
      |> glentities.decode(),
    input,
  )

  should.equal(
    input
      |> glentities.encode(glentities.Named)
      |> glentities.decode(),
    input,
  )

  should.equal(
    input
      |> glentities.encode(glentities.HTMLBody)
      |> glentities.decode(),
    input,
  )
}
